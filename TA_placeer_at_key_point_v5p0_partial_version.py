import vtk
import numpy as np
import os
import math
import SimpleITK as sitk
import subprocess
import time
import xlwt
import csv
import libcompute
import ctypes

ants_dir = r'C:\Novocure\Software\utils\ANTS_Install\bin'

#_compute = ctypes.CDLL('./lib/libcompute.so')

# Get the rotation matrix to rotate around axis 'axis' by theta radians
def rotation_matrix(axis, theta):

    axis = np.asarray(axis)
    axis = axis / math.sqrt(np.dot(axis, axis))
    a = math.cos(theta / 2.0)
    b, c, d = -axis * math.sin(theta / 2.0)
    aa, bb, cc, dd = a * a, b * b, c * c, d * d
    bc, ad, ac, ab, bd, cd = b * c, a * d, a * c, a * b, b * d, c * d
    return np.array([[aa + bb - cc - dd, 2 * (bc + ad), 2 * (bd - ac)],
                     [2 * (bc - ad), aa + cc - bb - dd, 2 * (cd + ab)],
                     [2 * (bd + ac), 2 * (cd - ab), aa + dd - bb - cc]])


# Create an outer surface of of a volumetric model
def create_model_outer_surface (model_filename):

    # read .nii file
    reader = vtk.vtkNIFTIImageReader()
    reader.SetFileName(model_filename)
    reader.Update()

    # apply a threshold and create a binary image
    threshold = vtk.vtkImageThreshold()
    threshold.SetInputConnection(reader.GetOutputPort())
    threshold.ThresholdByLower(0.5)  # remove all soft tissue
    threshold.ReplaceInOn()
    threshold.SetInValue(0)  # set all values below 400 to 0
    threshold.ReplaceOutOn()
    threshold.SetOutValue(1)  # set all values above 400 to 1
    threshold.Update()

    # apply marching cubes algorithm
    dmc = vtk.vtkDiscreteMarchingCubes()
    dmc.SetInputConnection(threshold.GetOutputPort())
    dmc.GenerateValues(1, 1, 1)
    dmc.ComputeAdjacentScalarsOff()
    dmc.Update()

    # get largest connected component
    connectivityFilter = vtk.vtkPolyDataConnectivityFilter()
    connectivityFilter.SetInputConnection(dmc.GetOutputPort())
    connectivityFilter.SetExtractionModeToLargestRegion()
    connectivityFilter.Update()

    # smooth surface. This is especially important to reduce geodesic distance error
    smoother = vtk.vtkWindowedSincPolyDataFilter()
    smoother.SetInputConnection(connectivityFilter.GetOutputPort())
    smoother.SetNumberOfIterations(20)
    smoother.BoundarySmoothingOff()
    smoother.FeatureEdgeSmoothingOff()
    smoother.SetFeatureAngle(30)
    smoother.SetPassBand(0.01)
    smoother.NonManifoldSmoothingOn()
    smoother.NormalizeCoordinatesOn()
    smoother.Update()

    polydata_to_image_matrix = reader.GetQFormMatrix()

    polydata_to_image_transform = vtk.vtkTransform()
    polydata_to_image_transform.SetMatrix(polydata_to_image_matrix)

    polydata_transform_filter = vtk.vtkTransformPolyDataFilter()
    polydata_transform_filter.SetTransform(polydata_to_image_transform)
    polydata_transform_filter.SetInputConnection(smoother.GetOutputPort())
    polydata_transform_filter.Update()

    # Save surface as STL file
    stlWriter = vtk.vtkSTLWriter()

    surface_model_filename = os.path.join(os.path.dirname(model_filename), 'head_model.stl')

    stlWriter.SetFileName(surface_model_filename)
    stlWriter.SetInputConnection(polydata_transform_filter.GetOutputPort())
    stlWriter.Write()

# find points that are connected with an edge to the point marked with id
def get_connected_points(mesh, id):

   connectedVertices = vtk.vtkIdList()
   cellIdList = vtk.vtkIdList()

   mesh.GetPointCells(id, cellIdList)

   for index in range (0, cellIdList.GetNumberOfIds()):
       pointIdList = vtk.vtkIdList()
       mesh.GetCellPoints(cellIdList.GetId(index), pointIdList)
       if (pointIdList.GetId(0) != id):
           connectedVertices.InsertNextId(pointIdList.GetId(0))
       else:
           connectedVertices.InsertNextId(pointIdList.GetId(1))

   return connectedVertices

# compte distance between point p and plane [q, qx, qy]
def get_distance_plane(p, q, q_x, q_y):

    n = np.cross(q_x, q_y)
    n = n / np.linalg.norm(n)

    distance = np.abs(np.dot((p - q), n))

    return distance

def project_point_to_line (a, b, p):
    ap = p-a
    ab = b-a
    projected_point = a + np.dot(ap,ab)/np.dot(ab,ab) * ab
    return projected_point

    # line = vtk.vtkLine()
    # return line.DistanceToLine(p, a, b)

def get_distance_to_cylender_direction (reference_point, direction ,points_locator, model_surface, radius):

    max_distance = 0
    min_distance = 0
    max_normal_distance = 5

    points_id = vtk.vtkIdList()

    points_locator.FindPointsWithinRadius(2*radius, reference_point, points_id)

    for index in range(0, points_id.GetNumberOfIds()):

        current_point = np.array(model_surface.GetPoint(points_id.GetId(index)))

        p = project_point_to_line(reference_point - 10 * direction, reference_point + 10 * direction, current_point)

        if (np.linalg.norm(p - current_point) < radius) and (np.linalg.norm(p - reference_point) < max_normal_distance):

            if (np.linalg.norm(p-reference_point) > 0):

                l1 = (p-reference_point)/np.linalg.norm(p-reference_point)
                l2 = direction/np.linalg.norm(direction)

                if np.dot(l1, l2) > 0:
                    if (np.linalg.norm(p - reference_point)) > max_distance:
                        max_distance = (np.linalg.norm(p - reference_point))
                else:
                    if (np.linalg.norm(p - reference_point)) > min_distance:
                        min_distance = (np.linalg.norm(p - reference_point))

    return max_distance, -min_distance

# compute geodesic distance from a given point upto a predefined distance
def compute_geodesic_distances (model_surface, points_locator, reference_point, max_distance, x_axis, y_axis, desired_distance, tolerance = 1.0):

    vertexDistances = vtk.vtkFloatArray()
    vertexDistances.SetNumberOfComponents(1)
    vertexDistances.SetName("geodesic_distance")

    keyNeighbors = vtk.vtkPoints()
    keyNeighbors.Initialize()

    # use a 'coloring' mechanism to ensure not repeating the same points
    for index in range(0, model_surface.GetNumberOfPoints()):
        vertexDistances.InsertNextValue(-1.0000)

    start_point_ID = points_locator.FindClosestPoint(reference_point)
    vertexDistances.SetValue(start_point_ID, 0.0)

    current_iteration_IDs = vtk.vtkIdList()
    current_iteration_IDs.Initialize()
    current_iteration_IDs.InsertNextId(start_point_ID)

    # estimate number of iteration needed to reach the max distance needed

    is_first_reference_point = True
    index_all = 1
    origin = [0, 0, 0]
    next_level_list_of_points = vtk.vtkIdList()

    while (current_iteration_IDs.GetNumberOfIds()>0):

        next_level_list_of_points.Reset()
        next_level_list_of_points.Initialize()

        for indexes_ID in range (0, current_iteration_IDs.GetNumberOfIds()):

            reference_point = model_surface.GetPoint(current_iteration_IDs.GetId(indexes_ID))

            if is_first_reference_point:
                origin = reference_point
                is_first_reference_point = False

            reference_distance = vertexDistances.GetValue(current_iteration_IDs.GetId(indexes_ID))

            connected_points_IDs = get_connected_points(model_surface, current_iteration_IDs.GetId(indexes_ID))

            for neighbour_ID_indexes in range(0, connected_points_IDs.GetNumberOfIds()):

                    current_point_ID = connected_points_IDs.GetId(neighbour_ID_indexes)

                    neighbor_point = model_surface.GetPoint(current_point_ID)

                    t1 = time.time()
                    # estimate geodesic distance of neighbour point
                    geodesic_distance_from_original_point = reference_distance + \
                                                            math.sqrt(vtk.vtkMath.Distance2BetweenPoints(neighbor_point, reference_point))

                    # if smaller distance exists, update to find shortest path
                    if (vertexDistances.GetValue(current_point_ID) > geodesic_distance_from_original_point):
                        # update neighbour's geodesic distance value
                        vertexDistances.SetValue(current_point_ID, geodesic_distance_from_original_point)

                    # if this point was not visited before (we pre-set all to -1)
                    if (vertexDistances.GetValue(current_point_ID) < 0):
                        # update neighbour's geodesic distance value
                        vertexDistances.SetValue(current_point_ID, geodesic_distance_from_original_point)

                        if geodesic_distance_from_original_point < max_distance:
                            # mark as a neighbour to compute its geodesic distance in next iteration
                            next_level_list_of_points.InsertNextId(current_point_ID)

                    # if the point is at a distance that is similar to a key point
                    if (np.abs(desired_distance - geodesic_distance_from_original_point) < tolerance):

                            # if the point is on the axial plane
                            distance = get_distance_plane(np.array(neighbor_point), origin, x_axis, y_axis)
                            if (distance < tolerance):
                                keyNeighbors.InsertNextPoint(neighbor_point)

        current_iteration_IDs.Reset()
        current_iteration_IDs.DeepCopy(next_level_list_of_points)

        index_all = index_all + 1

    return vertexDistances, keyNeighbors

# normalize the geodesic distances for visualization
def normalize_for_presentation(vertexDistances):

    vertexDistances_for_presentation = vtk.vtkFloatArray()
    vertexDistances_for_presentation.SetNumberOfComponents(1)
    vertexDistances_for_presentation.SetName("geodesic_distance")

    max_value = -1.0
    for current_point_ID in range(0, vertexDistances.GetNumberOfTuples()):
        if (vertexDistances.GetValue(current_point_ID) > max_value):
            max_value = vertexDistances.GetValue(current_point_ID)


    for current_point_ID in range(0, vertexDistances.GetNumberOfTuples()):
        if (vertexDistances.GetValue(current_point_ID) < 0): # if not computed it is because it is too far make it blue
            vertexDistances_for_presentation.InsertNextValue(1.0)
        else:
            vertexDistances_for_presentation.InsertNextValue(vertexDistances.GetValue(current_point_ID)/max_value)


    return vertexDistances_for_presentation

def statistics (signedDist):
    mean=0
    maxValue=-1000000
    minValue=1000000;
    variance=0
    standardDeviation=0
    first=True

    for  index in range (0, signedDist.GetNumberOfValues()):
        current_value=signedDist.GetValue(index);
        if(minValue > current_value and current_value!=0):
            minValue=current_value;
        if(maxValue < current_value and current_value<1):
            maxValue=current_value;
        if(first):
            mean=signedDist.GetValue(index);
            first=False;
            continue;
        
        mean_next=mean+ ( current_value - mean)/index;
        variance_next=variance + (current_value - mean)*(current_value - mean_next)
        mean=mean_next;
        variance=variance_next;
    
    if(variance !=0):
        standardDeviation=np.sqrt(variance/(signedDist.GetNumberOfValues()-1))
    print('min value:', minValue)
    print('max value:', maxValue)
    print('mean:', mean)
    print('standard deviation:', standardDeviation)
    print('calc numpy')
    
    data=np.array(signedDist.GetValue(0))
    for index in range (1, signedDist.GetNumberOfValues()):
        data=np.append(data, signedDist.GetValue(index))
        s=data.size
        
    mean=np.mean(data)
    standardDeviation=np.std(data)
    print('mean:', mean)
    print('standard deviation:', standardDeviation)


# present the model with the compute distances on it.
def present_mesh_and_computed_distances(model_surface, vertexDistances, layout_code):


    sphereMapper = vtk.vtkPolyDataMapper()
    sphereMapper.SetInputData(model_surface)
    sphereMapper.ScalarVisibilityOff()

    sphereActor = vtk.vtkActor()
    sphereActor.SetMapper(sphereMapper)
    sphereActor.GetProperty().SetOpacity(0.0)
    sphereActor.GetProperty().SetColor(1, 0, 0)

    points = model_surface.GetPoints()

    # Add distances to each point
    signedDistances = normalize_for_presentation(vertexDistances)
    
    csv_filename = os.path.join(r'/Users/idaymand/workspace/GeodesicDistance/Data/', 'result_'+layout_code+'.csv')
    with open(csv_filename, mode='w') as data_file:
        data_writer = csv.writer(data_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_NONNUMERIC)
        for index in range (0, signedDistances.GetNumberOfValues()):
            val=signedDistances.GetValue(index)
            data_writer.writerow([val])
 
#    statistics(signedDistances)
    polyData = vtk.vtkPolyData()
    polyData.SetPoints(points)
    polyData.GetPointData().SetScalars(signedDistances)

#    print output for tests
#    size_output=signedDistances.GetNumberOfValues()
#     count=0
#     size=0
#     str_output=''
#     for index in range(0, size_output):
#         val=signedDistances.GetValue(index);
#         if(val!=1.0):
#             str_output=str_output + 'index: '+ str(index)
#             str_output= str_output + ' value='+str(val)+ ' ';
#             size=size+1
#             count=count+1
#             if(count >10):
#                 count=0;
#                 print(str_output);
#                 str_output=''
#                 if (size >200):
#                     break

    vertexGlyphFilter = vtk.vtkVertexGlyphFilter()
    vertexGlyphFilter.SetInputData(polyData)
    vertexGlyphFilter.Update()

    signedDistanceMapper = vtk.vtkPolyDataMapper()
    signedDistanceMapper.SetInputConnection(vertexGlyphFilter.GetOutputPort())
    signedDistanceMapper.ScalarVisibilityOn()

    signedDistanceActor = vtk.vtkActor()
    signedDistanceActor.SetMapper(signedDistanceMapper)

    renderer = vtk.vtkRenderer()
    # renderer.AddViewProp(sphereActor)
    renderer.AddViewProp(signedDistanceActor)

    renderWindow = vtk.vtkRenderWindow()
    renderWindow.AddRenderer(renderer)

    renWinInteractor = vtk.vtkRenderWindowInteractor()
    renWinInteractor.SetRenderWindow(renderWindow)

    renderWindow.Render()
    renWinInteractor.Start()

# compute what is the maximal distance that should be considered here
def compute_max_distance_to_compute(layout_2D):

    min_vals = (np.min(layout_2D,axis=0))
    max_vals = (np.max(layout_2D,axis=0))

    return np.max(np.array(max_vals) - np.array(min_vals))*1.25


# place sphere on a set of points for debug
def place_spheres_on_points(points_3D, output_filename):

    all_spheres = vtk.vtkAppendPolyData()
    all_spheres.SetNumberOfInputs(points_3D.GetNumberOfPoints())

    for pointID in range(0, points_3D.GetNumberOfPoints()):

        source = vtk.vtkSphereSource()
        source.SetCenter(points_3D.GetPoint(pointID))
        source.SetRadius(1)
        source.Update()

        input = vtk.vtkPolyData()
        input.ShallowCopy(source.GetOutput())

        all_spheres.AddInputData(input)

    all_spheres.Update()

    stlWriter = vtk.vtkSTLWriter()
    stlWriter.SetFileName(output_filename)
    stlWriter.SetInputConnection(all_spheres.GetOutputPort())
    stlWriter.Write()

# compute the rotation matrix of rotating [0, 1, 0] to target vector
def get_rotation_from_y_axis (target_vector):

    if (np.abs(target_vector[2])<0.000000000001):
        target_vector[2] = 0.000000000001

    rotation_matrix = np.zeros((3,3))

    rotation_matrix[0, 0] = -1.0
    rotation_matrix[1, 0] = -1.0
    rotation_matrix[2, 0] = (target_vector[0] + target_vector[1])/target_vector[2]

    rotation_matrix[0, 1] = target_vector[0]
    rotation_matrix[1, 1] = target_vector[1]
    rotation_matrix[2, 1] = target_vector[2]

    rotation_matrix[:, 2] = np.cross(rotation_matrix[:, 0], rotation_matrix[:, 1])

    rotation_matrix[:, 0] = rotation_matrix[:, 0] / np.linalg.norm(rotation_matrix[:, 0])
    rotation_matrix[:, 2] = rotation_matrix[:, 2] / np.linalg.norm(rotation_matrix[:, 2])

    return rotation_matrix



def get_points_from_label_image(transformed_axes_filename):

    key_points = []

    transformed_axes_image = sitk.ReadImage(transformed_axes_filename)
    number_of_labels = int(np.max(sitk.GetArrayFromImage(transformed_axes_image)))

    reader = vtk.vtkNIFTIImageReader()
    reader.SetFileName(transformed_axes_filename)
    reader.Update()

    threshold = vtk.vtkImageThreshold()
    dmc = vtk.vtkDiscreteMarchingCubes()
    cleanPolyData = vtk.vtkCleanPolyData()
    centerOfMassFilter = vtk.vtkCenterOfMass()

    for label in range(1, number_of_labels + 1):

        threshold.SetInputConnection(reader.GetOutputPort())
        threshold.ThresholdBetween(label-0.1, label+0.1)  # remove all soft tissue
        threshold.ReplaceInOn()
        threshold.SetInValue(1)  # set all values below 400 to 0
        threshold.ReplaceOutOn()
        threshold.SetOutValue(0)  # set all values above 400 to 1
        threshold.Update()

        dmc.SetInputConnection(threshold.GetOutputPort())
        dmc.GenerateValues(1, 1, 1)
        dmc.Update()

        polydata_to_image_matrix = reader.GetQFormMatrix()

        polydata_to_image_transform = vtk.vtkTransform()
        polydata_to_image_transform.SetMatrix(polydata_to_image_matrix)

        polydata_transform_filter = vtk.vtkTransformPolyDataFilter()
        polydata_transform_filter.SetTransform(polydata_to_image_transform)
        polydata_transform_filter.SetInputConnection(dmc.GetOutputPort())
        polydata_transform_filter.Update()

        cleanPolyData.SetInputConnection(polydata_transform_filter.GetOutputPort())
        cleanPolyData.Update()

        centerOfMassFilter.SetInputConnection(cleanPolyData.GetOutputPort())
        centerOfMassFilter.Update()

        center = np.array(centerOfMassFilter.GetCenter())

        key_points.append(center)

    return key_points



def compute_global_coordinate_from_phantom(model_filename, transform_filename, axes_atlas_filename, is_not_on_axes_flag=False):

    transformed_axes_filename = os.path.join(os.path.dirname(model_filename), 'transformed_' + os.path.basename(model_filename))

 #   cmd = [os.path.join(ants_dir, "WarpImageMultiTransform"), "3", axes_atlas_filename,
 #          transformed_axes_filename, transform_filename, "-R", model_filename, '--use-NN']
 #   subprocess.run(cmd)

  #  cmd = [os.path.join(ants_dir, "WarpImageMultiTransform"), "3", r'C:\Novocure\Software\segmentation\data\colin27_t1_tal_lin.nii',
   #        os.path.join(os.path.dirname(transformed_axes_filename), 'colins_transformed.nii.gz'), transform_filename, "-R", model_filename, '--use-NN']
  #  subprocess.run(cmd)

    if (is_not_on_axes_flag):
        global_origin = np.array([128, 128, 90])
        global_axes = np.identity(3)
        key_TTF_locations = []

    else:
        key_points = get_points_from_label_image(transformed_axes_filename)

        global_origin = np.array(key_points[0])

        z_axis = np.array(key_points[3]) - np.array(key_points[0])
        z_axis = z_axis / np.linalg.norm(z_axis)

        perp_plan_vec1 = np.array([-1.0, -1.0, (z_axis[0] + z_axis[1]) / z_axis[2]])
        perp_plan_vec2 = np.cross(z_axis, perp_plan_vec1)

        x_axis = np.array(key_points[2]) - np.array(key_points[0])
        x_axis = project_3d_vector_on_plane(x_axis, perp_plan_vec1, perp_plan_vec2)
        x_axis = x_axis/np.linalg.norm(x_axis)

        y_axis = np.cross(z_axis, x_axis)
        y_axis = y_axis / np.linalg.norm(y_axis)

        global_axes = np.vstack((x_axis, y_axis, z_axis))



        key_TTF_locations = np.array(key_points[4:12])

    return global_origin, global_axes, key_TTF_locations


def get_default_3D_orientation(reference_point, polydata, zero_axis):


    pointNormalsRetrieved = (polydata.GetPointData().GetNormals())

    point_ID = polydata.FindPoint(reference_point)

    rotation_axis = np.array(pointNormalsRetrieved.GetTuple(point_ID))

    if (np.abs(rotation_axis[2]) < 0.000000000000001):
        rotation_axis[2] = 0.000000000000001

    rotation_axis = rotation_axis/np.linalg.norm(rotation_axis)

    x_direction = np.array([-1.0, -1.0, ((rotation_axis[0] + rotation_axis[1])/(rotation_axis[2]))])
    x_direction = x_direction/np.linalg.norm(x_direction)

    y_direction = np.cross(rotation_axis, x_direction)
    y_direction = y_direction/np.linalg.norm(y_direction)

    zero_axis_projection = project_3d_vector_on_plane(zero_axis, x_direction, y_direction)

    up_x_direction = zero_axis_projection / np.linalg.norm(zero_axis_projection)
    up_y_direction = np.cross(rotation_axis, up_x_direction)

    return up_x_direction, up_y_direction, rotation_axis

def rotate_axes(x_direction, y_direction, rotation_axis, angle_degs):

    rotated_y_direction = np.dot(rotation_matrix(rotation_axis, angle_degs * (np.pi / 180.0)), y_direction)
    rotated_x_direction = np.dot(rotation_matrix(rotation_axis, angle_degs * (np.pi / 180.0)), x_direction)

    return rotated_x_direction, rotated_y_direction


def shift_points_along_direction(points_3D, directions_3D, shift_size):

    shifted_points = vtk.vtkPoints()
    shifted_points.Initialize()

    for index in range(0, points_3D.GetNumberOfPoints()):

        current_point = np.array(points_3D.GetPoint(index))
        shift = np.array(directions_3D.GetTuple(index))

        current_shifted_point = current_point + (shift_size[index]*shift)

        shifted_points.InsertNextPoint(current_shifted_point)

    return shifted_points


def project_3d_vector_on_plane (p, x, y):
    x = x / np.linalg.norm(x)
    y = y / np.linalg.norm(y)

    px = x*np.dot(x,p)
    py = y*np.dot(y,p)

    v = px+py

    if np.max(np.isnan(v)):
        print ('%%% Reference point is too close to z-axis, using default axes instead of local ones')
        v = x+y
        v[0], v[1] = v[1], v[0]

    return v


def measure_angular_error(layout_points_3D, x_direction, y_direction):

    layout_direction = np.array(layout_points_3D.GetPoint(1)) - np.array(layout_points_3D.GetPoint(2))
    layout_direction = layout_direction / np.linalg.norm(layout_direction)

    layout_inplane_direction = project_3d_vector_on_plane(layout_direction, x_direction, y_direction)
    layout_inplane_direction = layout_inplane_direction / np.linalg.norm(layout_inplane_direction)

    cos_of_angle = np.dot(layout_direction, layout_inplane_direction)

    angular_error = np.arccos(np.clip(cos_of_angle, -1, 1)) * 180 / np.pi

    return (angular_error)

def measure_angular_error_with_normal (layout_points_3D, x_direction, y_direction):
    layout_direction = np.array(layout_points_3D.GetPoint(1)) - np.array(layout_points_3D.GetPoint(2))
    layout_direction = layout_direction / np.linalg.norm(layout_direction)

    normal_to_plane = np.cross(x_direction, y_direction)
    normal_to_plane = normal_to_plane / np.linalg.norm(normal_to_plane)

    cos_of_angle = np.dot(normal_to_plane, layout_direction)
    angular_error = np.abs(90 - np.arccos(np.clip(cos_of_angle, -1, 1)) * 180 / np.pi)

    return (angular_error)


def get_points_at_plane (model_filename, reference_point, plane_x_axis, plane_y_axis, planename = 'plane.stl',
                         size_of_plane = 600, step = 50):

    points_on_plane = vtk.vtkPoints()
    points_on_plane.Initialize()

    axes_sum_vector = plane_x_axis + plane_y_axis
    origin = np.array(reference_point) - int((size_of_plane/2))*(axes_sum_vector)

    for x_location in range(0, int(size_of_plane/step)):
        for y_location in range(0, int(size_of_plane/step)):
            current_location = origin + step*(x_location*plane_x_axis + y_location*plane_y_axis)
            points_on_plane.InsertNextPoint(current_location.tolist())

    number_of_points = points_on_plane.GetNumberOfPoints()

    place_spheres_on_points(points_on_plane, os.path.join(os.path.dirname(model_filename), 'points'+planename))

    plane_vtk = vtk.vtkPolyData()
    plane_vtk.SetPoints(points_on_plane)

    triangulate = vtk.vtkDelaunay2D()
    triangulate.SetInputData(plane_vtk)
    triangulate.SetTolerance(0.001)
    triangulate.SetAlpha(step*2)
    triangulate.BoundingTriangulationOff()
    triangulate.Update()

    stlWriter = vtk.vtkSTLWriter()
    stlWriter.SetFileName(os.path.join(os.path.dirname(model_filename), planename))
    stlWriter.SetInputConnection(triangulate.GetOutputPort())
    stlWriter.Write()

    return 0 # intersectionPolyDataFilter.GetOutput()


start = time.time()



def find_closest_point(point, points_3D, directions_3D):

    min_distance = 100000
    closest_point = []
    direction = []

    for pointID in range(0, points_3D.GetNumberOfPoints()):

        current_point = points_3D.GetPoint(pointID)
        current_distance = np.linalg.norm(np.array(point) - np.array(current_point))

        # if distance is smaller, but not at the same point
        if (current_distance < min_distance) and (current_distance > 0.001):
            closest_point = current_point
            direction = directions_3D.GetTuple(pointID)
            min_distance = current_distance

    return closest_point, direction





def place_TA (reference_point, layout_2D, model_surface, obbTree, points_locator, global_origin, global_axes, angle_degs, layout_code,
              directory, gel_size_above_skin, ceramic_size, plate_size, computeGelHeight = True, geodesic_weight_array = [0.3, 0.2, 0.1]):




    ### compute geodesic distances:

    # compute bounds to save time of geodesic computing
    distance_between_key_central_and_key_TAs = np.max(layout_2D[1] - layout_2D[0])
    max_distance = compute_max_distance_to_compute(layout_2D)

    start = time.time()
    
    geo1= libcompute.geodesic()
    geo1.SetPolyData(model_surface)
    array_type = ctypes.c_float * 3
    x_axes=global_axes[0]
    y_axes=global_axes[1]
    
    geo1.compute_geodesic_distances(array_type(*reference_point), ctypes.c_float(max_distance), array_type(*x_axes), array_type(*y_axes), ctypes.c_float(distance_between_key_central_and_key_TAs), 0.5)
    
    vertexDistances=geo1.GetVertexDistances()
    key_TA_Neighbours=geo1.GetKeyNeighbours()
  #  vertexDistances, key_TA_Neighbours = compute_geodesic_distances(model_surface, points_locator, reference_point,
  #                                                                  max_distance=max_distance,
  #                                                                  x_axis=global_axes[0], y_axis=global_axes[1],
  #                                                                  desired_distance=distance_between_key_central_and_key_TAs,
  #                                                                  tolerance=0.5)
    end = time.time()

    print ('geodesic distance time: ', end-start)
    # For debug:
    present_mesh_and_computed_distances(model_surface, vertexDistances, layout_code)
    print("\n")
        
    place_spheres_on_points(key_TA_Neighbours, os.path.join(directory, 'Intersection_points_' + layout_code + '.stl'))



class TA_placer():

    def __init__(self, directory, model_image_basename='head_model.nii.gz'):

        self.vtk_object = vtk.vtkObject()
        self.vtk_object.GlobalWarningDisplayOff()

        self.directory = directory

        model_image_filename = os.path.join(directory, model_image_basename)

        # if model image is not parallel to the axes, create a new image that is parallel to the axes
        is_not_on_axes_flag = self.is_not_on_axes (model_image_filename)

        self.model_filename = model_image_filename

        # fixed algorithm's dataset
        algorithm_folder = r'C:\Novocure\Research\10_TA_placement\alg_data'
        transform_file = os.path.join(directory, 'atlas_afine_in_T1_coordsAffine.txt')
        axes_atlas = os.path.join(algorithm_folder, 'key_locations_parallel_v3.nii.gz')

        # Generate head surface

        create_model_outer_surface(model_image_filename)
        model_filename = os.path.join(directory, 'head_model.stl')

        # read the model
        stlReader = vtk.vtkSTLReader()
        stlReader.SetFileName(model_filename)
        stlReader.Update()

        normal_computer = vtk.vtkPolyDataNormals()
        normal_computer.SetInputConnection(stlReader.GetOutputPort())
        normal_computer.ComputeCellNormalsOff()
        normal_computer.ComputePointNormalsOn()
        normal_computer.FlipNormalsOff()
        normal_computer.NonManifoldTraversalOn()
        normal_computer.AutoOrientNormalsOn()
        normal_computer.Update()


        self.model_surface = normal_computer.GetOutput()

        # use an efficient ray tracing algorithm
        # (currently computed on CPU, can be accelerated using GPU with VTK-m)
        self.obbTree = vtk.vtkOBBTree()
        self.obbTree.SetDataSet(self.model_surface)
        self.obbTree.BuildLocator()

        self.points_locator = vtk.vtkPointLocator()
        self.points_locator.SetDataSet(self.model_surface)
        self.points_locator.AutomaticOn()
        self.points_locator.SetNumberOfPointsPerBucket(10)
        self.points_locator.BuildLocator()

        # Get global coordinates and a list of reference points
        self.global_origin, self.global_axes, self.key_TTF_locations = compute_global_coordinate_from_phantom(model_image_filename,
                                                                                                              transform_file,
                                                                                                              axes_atlas,
                                                                                                              is_not_on_axes_flag)
        self.names_code = []
        self.distance_errors = []
        self.angular_errors = []
        self.gel_distance_errors = []
        self.computation_times = []

        self.last_TA = []

    def place_multiple_TAs(self, layout_2D, layout_code, angles_of_layouts, gel_size_above_skin, ceramic_size,
                           plate_size, reference_points=[], computeGelHeight=True, geodesic_weight_array=[0.3, 0.2, 0.1]):

        if len(reference_points) < 1:
            reference_points = self.key_TTF_locations

        # Loop over reference points
        for index in range(0, len(reference_points)):
            reference_point = np.array(reference_points[index])

            # For debug - create an axial plane
            # intersection = get_points_at_plane(self.model_filename, reference_point, self.global_axes[0], self.global_axes[1])
            print (' ')
            print ('reference point ', index, reference_point)
            
            start_single = time.time()
            place_TA(reference_point, layout_2D, self.model_surface, self.obbTree, self.points_locator, self.global_origin, self.global_axes,
                                                     angles_of_layouts[index], layout_code[index],
                                                     self.directory, gel_size_above_skin, ceramic_size, plate_size, computeGelHeight=computeGelHeight,
                                                      geodesic_weight_array=geodesic_weight_array)
            end_single = time.time()


    def is_not_on_axes(self, model_image_filename):

        eps = 0.00000001

        image_reader = vtk.vtkNIFTIImageReader()
        image_reader.SetFileName(model_image_filename)
        image_reader.Update()

        matrix = image_reader.GetQFormMatrix()

        for index1 in range(0, 3):
            for index2 in range(0, 3):
                if ((index1 != index2) and (abs(matrix.GetElement(index1, index2)) > eps)):
                    return True

        return False

