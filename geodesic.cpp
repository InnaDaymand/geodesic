//============================================================================
// Name        : geodesic.cpp
// Author      : Inna Daymand
// Version     :
// Copyright   : Free
// Description : Geodesic Distance in C++, dynamic library for Python
//============================================================================


#include <python.hpp>
#include <to_python_converter.hpp>
#include <detail/wrap_python.hpp>
#include <numpy.hpp>

#include <vtkObjectBase.h>
#include <vtkPointLocator.h>
#include <vtkPolyData.h>
#include <vtkFloatArray.h>
#include <vtkPoints.h>
#include <vtkVector.h>

using namespace boost::python;

template<class Ptr>
struct vtkObjectPointer_to_python
{
	static PyObject* convert(const Ptr &p)
	{
		if(p == NULL)
		{
			return incref(Py_None);
		}
		std::ostringstream oss;
		oss << (vtkObjectBase*) p; // here don't get address
		std::string address_str = oss.str();

		boost::python::object obj = boost::python::import("vtk").attr("vtkObjectBase")(address_str);
		return boost::python::incref(obj.ptr());
	}
};

void* extract_vtk_wrapped_pointer(PyObject* obj)
{
    char thisStr[] = "__this__";
    //first we need to get the __this__ attribute from the Python Object
    if (!PyObject_HasAttrString(obj, thisStr))
        return NULL;

    PyObject* thisAttr = PyObject_GetAttrString(obj, thisStr);
    if (thisAttr == NULL)
        return NULL;
    PyObject* temp_bytes=PyUnicode_AsEncodedString(thisAttr, "UTF-8", "strict");
    const char* str=0;
    if(temp_bytes !=NULL){
    	str=PyBytes_AS_STRING(temp_bytes); // Borrowed pointer
    	str=strdup(str);
        boost::python::decref(temp_bytes);
    }
//    const char* str = PyString_AsString(thisAttr);
    if(str == 0 || strlen(str) < 1)
        return NULL;
    char hex_address[32], *pEnd;
    char *_p_ = (char*)strstr(str, "_p_vtk");
    if(_p_ == NULL) return NULL;
    char *class_name = strstr(_p_, "vtk");
    if(class_name == NULL) return NULL;
    strcpy(hex_address, str+1);
    hex_address[_p_-str-1] = '\0';

    long address = strtol(hex_address, &pEnd, 16);

    vtkObjectBase* vtk_object = (vtkObjectBase*)((void*)address);
    if(vtk_object->IsA(class_name))
    {
        return vtk_object;
    }

    return NULL;
}


struct geodesicData{
     vtkFloatArray* mVertexDistances;
     vtkPoints* mKeyNeighbours;

     geodesicData(){mVertexDistances=NULL; mKeyNeighbours=NULL;};
};


struct geodesic
{
	geodesic();
	~geodesic();
	void SetPolyData(vtkPolyData * mesh);
	void SetGeodesicData();
	vtkFloatArray * GetVertexDistances() { return mVertexDistances; }
	vtkPoints * GetKeyNeighbours() { return mKeyNeighbours; }
	vtkFloatArray * mVertexDistances;
	vtkPoints* mKeyNeighbours;
	vtkPolyData* mMesh;
	vtkPointLocator* mPLocator;
	vtkIdList* mConnectedVertices;
	vtkIdList* mCellIdList;
	geodesicData mResultData;

	void deleteObjects();

	void compute_geodesic_distances (float* ireference_point,
			float max_distance,
			float* ix_axes, float* iy_axes
			, float desired_distance, float tolerance);

//	geodesicData* compute_geodesic_distances (double* reference_point, double max_distance,
//			double* x_axis, double* y_axis
//			, double desired_distance, double tolerance);

	void get_connected_points(int id);
	double get_distance_plane(double*, double*, double*, double*);
	void SetReferencePoint(vtkPoints*);
	vtkPoints* reference_point;

};

geodesic::geodesic()
{
	mVertexDistances = vtkFloatArray::New();
	mVertexDistances->Initialize();

  	mKeyNeighbours=vtkPoints::New();
  	mKeyNeighbours->Initialize();

  	mConnectedVertices=vtkIdList::New();
  	mConnectedVertices->Initialize();
  	mCellIdList=vtkIdList::New();
  	mCellIdList->Initialize();

  	mMesh= vtkPolyData::New();
  	mMesh->Initialize();

  	mPLocator=vtkPointLocator::New();
  	mPLocator->Initialize();

  	reference_point=vtkPoints::New();
  	reference_point->Initialize();


//  	mResultData.mKeyNeighbours=mKeyNeighbours;
//  	mResultData.mVertexDistances=mVertexDistances;
}

geodesic::~geodesic()
{
	deleteObjects();
}

void geodesic::deleteObjects()
{
	if(mVertexDistances){
		mVertexDistances->Delete();
		mVertexDistances = NULL;
	}
	if(mKeyNeighbours){
		mKeyNeighbours->Delete();
		mKeyNeighbours=NULL;
	}
	if(mConnectedVertices){
		mConnectedVertices->Delete();
		mConnectedVertices=NULL;
	}
	if(mCellIdList){
		mCellIdList->Delete();
		mCellIdList=NULL;
	}
	if(mPLocator){
		mPLocator->Delete();
		mPLocator=NULL;
	}
	if(mMesh){
		mMesh->Delete();
		mMesh=NULL;
	}
}

void geodesic::SetGeodesicData(){
	mResultData.mVertexDistances=mVertexDistances;
	mResultData.mKeyNeighbours=mKeyNeighbours;
}

void geodesic::SetReferencePoint(vtkPoints* ptr){
	reference_point->Reset();
	reference_point->Initialize();
	reference_point->DeepCopy(ptr);
}

void geodesic::SetPolyData(vtkPolyData *mesh)
{
    mMesh= mesh;
    std::cout<< "mesh \n" << mMesh;
    std::cout<< "locator \n" << mPLocator;
    mPLocator->SetDataSet(mesh);
    std::cout<< "locator SetDataSet \n";
    mPLocator->AutomaticOn();
    std::cout<< "locator AutomaticOn \n";
    mPLocator->SetNumberOfPointsPerBucket(10);
    std::cout<< "locator setNumberOfPoints \n";
    mPLocator->BuildLocator();
    std::cout<< "locator BuildLocator \n";
}


/*geodesicData* geodesic::compute_geodesic_distances(double* ireference_point, double max_distance,
		double* ix_axis, double* iy_axis
		, double desired_distance, double tolerance){
	compute_geodesic_distances_base(ireference_point, max_distance, ix_axis, iy_axis, desired_distance, tolerance);
	SetGeodesicData();
	return &mResultData;
}*/

void geodesic::compute_geodesic_distances (float* ireference_point,
		float max_distance,
		float* ix_axes, float* iy_axes
		, float desired_distance, float tolerance){

	std::cout<<"Entrance \n";
	mVertexDistances->Reset();
	std::cout<<"mVertexDistances -> Reset()\n";
	mVertexDistances->Initialize();
	std::cout<<"mVertexDistances -> Initialize()\n";
    mVertexDistances->SetNumberOfComponents(1);
	std::cout<<"mVertexDistances -> SetNumberOfComponents()\n";
    mVertexDistances->SetName("geodesic_distance");
	std::cout<<"mVertexDistances -> SetName()\n";

//	vtkPoints* rf=vtkPoints::New();
//	rf->Initialize();
//	std::cout<<"begin deep copy count=" << ireference_point->GetNumberOfPoints()<<"\n";
//	rf->DeepCopy(ireference_point);
	std::cout<<"ireference_point "<<reference_point<<"\n";
	std::cout<<"ireference_point  count="<<reference_point->GetNumberOfPoints()<<"\n";
	double* refp=reference_point->GetPoint(0);
	std::cout<<"reference_point pointer" <<"\n";

//    vtkVector3d reference_point(refp);
/*	std::cout<<"reference_point vector" << refp<<"\n";
    vtkVector3d x_axes(ix_axes->GetPoint(0));
    vtkVector3d y_axes(iy_axes->GetPoint(0));

	std::cout<<"vectors initialization\n";

    mKeyNeighbours->Reset();
	std::cout<<"mKeyNeighbours -> Reset()\n";
    mKeyNeighbours->Initialize();
	std::cout<<"mKeyNeighbours -> Initialize()\n";

    // use a 'coloring' mechanism to ensure not repeating the same points
    for (int index=0; index <  mMesh->GetNumberOfPoints(); index++)
        mVertexDistances->InsertNextValue(-1.0000);
	std::cout<<"mVertexDistances -> InsertNextValue()\n";

//    reference_point.Set(10.9,76.7, 70.7);
    int start_point_ID = mPLocator->FindClosestPoint(reference_point.GetData());
    mVertexDistances->SetValue(start_point_ID, 0.0);

    vtkSmartPointer<vtkIdList>current_iteration_IDs = vtkSmartPointer<vtkIdList>::New();
    current_iteration_IDs->Initialize();
    current_iteration_IDs->InsertNextId(start_point_ID);

    // estimate number of iteration needed to reach the max distance needed

    bool is_first_reference_point = true;
    int index_all = 1;
    double origin[3] = {0, 0, 0};
    vtkSmartPointer<vtkIdList> next_level_list_of_points = vtkSmartPointer<vtkIdList>::New();

    while (current_iteration_IDs->GetNumberOfIds()>0){

        next_level_list_of_points->Reset();
        next_level_list_of_points->Initialize();

        for( int indexes_ID=0; indexes_ID < current_iteration_IDs->GetNumberOfIds(); indexes_ID++){

        	double* point=mMesh->GetPoint(current_iteration_IDs->GetId(indexes_ID));
            reference_point.Set(point[0], point[1], point[2]);
            if (is_first_reference_point){
                origin[0] = reference_point.GetX(), origin[1]=reference_point.GetY(), origin[2]=reference_point.GetZ();
                is_first_reference_point =false;
            }

            double reference_distance = mVertexDistances->GetValue(current_iteration_IDs->GetId(indexes_ID));

            get_connected_points(current_iteration_IDs->GetId(indexes_ID));

            for (int neighbour_ID_indexes=0;neighbour_ID_indexes< mConnectedVertices->GetNumberOfIds();
            		neighbour_ID_indexes++ ){

				int current_point_ID = mConnectedVertices->GetId(neighbour_ID_indexes);

				double* neighbor_point = mMesh->GetPoint(current_point_ID);

				//estimate geodesic distance of neighbour point
				double geodesic_distance_from_original_point = reference_distance +
						sqrt(vtkMath::Distance2BetweenPoints(neighbor_point, reference_point.GetData()));

//                    if smaller distance exists, update to find shortest path
				if (mVertexDistances->GetValue(current_point_ID) > geodesic_distance_from_original_point){
					// update neighbour's geodesic distance value
					mVertexDistances->SetValue(current_point_ID, geodesic_distance_from_original_point);
				}

//                     if this point was not visited before (we pre-set all to -1)
				if (mVertexDistances->GetValue(current_point_ID) < 0){
					// update neighbour's geodesic distance value
					mVertexDistances->SetValue(current_point_ID, geodesic_distance_from_original_point);

					if (geodesic_distance_from_original_point < max_distance){
						// mark as a neighbour to compute its geodesic distance in next iteration
						next_level_list_of_points->InsertNextId(current_point_ID);
					}
				}

//                     if the point is at a distance that is similar to a key point
				if (abs(desired_distance - geodesic_distance_from_original_point) < tolerance){

//                             if the point is on the axial plane
						double distance = get_distance_plane(neighbor_point, origin, x_axes.GetData(), y_axes.GetData());
						if (distance < tolerance){
							mKeyNeighbours->InsertNextPoint(neighbor_point);
						}
				}
            }
        }

        current_iteration_IDs->Reset();
        current_iteration_IDs->DeepCopy(next_level_list_of_points);

        index_all = index_all + 1;
    }*/
}


void geodesic::get_connected_points(int id){

   mConnectedVertices->Reset();
   mConnectedVertices->Initialize();
   mCellIdList->Reset();
   mCellIdList->Initialize();

   mMesh->GetPointCells(id, mCellIdList);

   vtkIdList* pointIdList = vtkIdList :: New();
   for (int index=0; index < mCellIdList->GetNumberOfIds(); index++){
	   pointIdList->Reset();
       mMesh->GetCellPoints(mCellIdList->GetId(index), pointIdList);
       if (pointIdList->GetId(0) != id)
           mConnectedVertices->InsertNextId(pointIdList->GetId(0));
       else
           mConnectedVertices->InsertNextId(pointIdList->GetId(1));
   }
}

// compute distance between point p and plane [q, qx, qy]
double geodesic::get_distance_plane(double* p, double* q, double* q_x, double* q_y){

	double n[3]={0,0,0};
    vtkMath::Cross(q_x, q_y, n);
    double normn=vtkMath::Norm(n);
    n[0] = n[0] / normn;
    n[1] = n[1] / normn;
    n[2] = n[2] / normn;
    double sub[3]={0,0,0};

    vtkMath::Subtract(p, q, sub);
    double distance = abs(vtkMath::Dot(sub, n));

    return distance;
}
/* register the to-python converter */
/* register the from-python converter */
#define VTK_PYTHON_CONVERSION(type) \
		boost::python::to_python_converter<type*, vtkObjectPointer_to_python<type*> >();  \
		boost::python::converter::registry::insert(&extract_vtk_wrapped_pointer, boost::python::type_id<type>())


// Create Python Module, with converters and geodesic  wrapped

BOOST_PYTHON_MODULE(libcompute)
{
    using namespace boost::python;
	VTK_PYTHON_CONVERSION (vtkFloatArray);
	VTK_PYTHON_CONVERSION (vtkPoints);
	VTK_PYTHON_CONVERSION (vtkPolyData);


	boost::python::class_<geodesic>("geodesic")
		.def("GetVertexDistances",&geodesic::GetVertexDistances, boost::python::return_value_policy<boost::python::return_by_value>())
		.def("SetPolyData",&geodesic::SetPolyData,"Set mesh")
		.def("SetReferencePoint", &geodesic::SetReferencePoint, "Set reference point")
		.def("GetKeyNeighbours",&geodesic::GetKeyNeighbours,boost::python::return_value_policy<boost::python::return_by_value>())
		.def("compute_geodesic_distances",&geodesic::compute_geodesic_distances,"compute geodesic distance");
}

